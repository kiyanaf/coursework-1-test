//using ros and standard messages for type string and int
#include <ros.h>
#include <std_msgs/Int32.h>
#include <std_msgs/String.h>


// buzzer pin
#define buzzer 3


// accelerometer pin
#define accelerometer A6


// define pin numbers of UltraSonic
const int trigPin  = 9;
const int echoPin  = 10;


//LED pins

const int green = 6;
const int yellow = 7;


// creating a ros node
ros::NodeHandle nh;



// a variable to hold the data from rosserial
int var;



//publisher to chatter
std_msgs::String str_msg;
ros::Publisher chatter("chatter", &str_msg);



// call back function to receive data from ros
void messageCb( const std_msgs::Int32 &msg)
{
  // assigning variable to receive that int from ros
  var = msg.data;

  // measuring the distance in cm
  int distance =  measure (trigPin, echoPin);

  // angle measurements
  int angle = analogRead(accelerometer);
  // angle digitazation to 5 values from -90 to 90 with step of 45 degrees
  angle = map (angle, 400, 630, 0, 5);
  angle = (angle - 2) * 45;

  // processing the user request
  /*
     The input from user can be 1. Forward 2.Turn Left 3. Turn Right
     According to the current distance ahead and the current angle of the vehicle
     the system will respond with a green LED indicating OK or a yellow LED indicating
     with caution and a buzzer indicating High risk.

  */
  
  if (distance < 7) {
    error();
  }
  else
  {

    switch (angle) {

      case 0: {
          // green led for any case
          BlinkLed(green);
          break;
        }

      case -45: {
          if (var == 1 || var == 3) {
            BlinkLed(green);
          }

          else if (var == 2) {
            BlinkLed(yellow);

          }

          break;
        }

      case -90: {
          error();
          break;
        }

      case 90: {
          error();
          break;
        }

      case 45: {
          if (var == 1 || var == 2) {
            BlinkLed(green);
          }

          else if (var == 3) {
            BlinkLed(yellow);

          }
          break;
        }



    }


  }

}

ros::Subscriber<std_msgs::Int32> sub("my_topic", &messageCb );

void setup()
{

  nh.initNode();
  nh.subscribe(sub);
  nh.advertise(chatter);
  pinMode(buzzer, OUTPUT);
  pinMode(trigPin, OUTPUT);  // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
  pinMode(accelerometer, INPUT);//Accelerometer as input
  pinMode(green, OUTPUT);
  pinMode(yellow, OUTPUT);
  Serial.begin(57600);
}

void loop()
{
  //float distance;
  int angle;

  //read ultrasonic distamce
  int distance = measure (trigPin, echoPin);

  //read the angle from accelerometer
  angle = analogRead(accelerometer);

  angle = map (angle, 400, 630, 0, 5);

  angle = (angle - 2) * 45;

  // convert angle to string
  char ang[10];
  itoa(angle, ang, 10);

  //convert to string
  char dist[10];
  itoa(distance, dist, 10);


  char out[40] = "Angle(deg): ";



  //concatanate the data of distance and angle
  strcat(out, ang);
  strcat(out, " | distance(cm): ");
  strcat(out, dist);


  // test printing on serial monitor
  //Serial.print("Angle :");
  //Serial.println(out);


  //send data
  str_msg.data = out;
  chatter.publish( &str_msg );
  nh.spinOnce();


  delay(1000);

}


void error() {
  for ( int i = 0; i < 3; i++) {
    tone(buzzer, 1000); // Send 1KHz sound signal...
    delay(1000);        // ...for 1 sec
    noTone(buzzer);     // Stop sound...
    delay(1000);        // ...for 1sec
  }

}

// a function that calculates the distance
int measure (int trigPin, int echoPin) {
  long duration;
  int distance;

  // Clears the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);

  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);

  // Calculating the distance in cm
  distance = duration * 0.034 / 2 ;
  //publishing data


  delay(100);
  return distance;
}


void BlinkLed (const int pin) {
  digitalWrite(pin, HIGH);
  delay(1000);
  digitalWrite(pin, LOW);

}
