
Author : Kiyan Afsari 
Student ID: M00693546 
Middlesex university of Dubai
MSc Robotics | PED4420 | Dr.Sotirios Spanogiannopoulos

------------------------------------ Course Work 1 ------------------------------------

--- Hardware system:

The following program is designed to simulate an IMU alarming system on a mobile robot. The hardware used in this simulation are listed below. 1. Arduino Nano (processor): The microcontroller is responsible for collecting the data from the sensors and combining them into a meaningful data and publishing it to rosserial. 2. Ultrasonic HC-SR04: This module calculates the distance by sending a 40kHz signal and measuring the time of its return. Then dividing the time by the speed of the sound to obtain the distance. 3. ADXL335 Lilypad Accelerometer: The 3D accelerometer is used to mesure the tilting of the vehicle.In this simulation one axis of the accelrometer (x-axis) is used. 4. Piezo buzzer: used as an alarm system. when the condition of the system is critical the buzzer will produce an alarm indicating critical condition. 




--- Ros Serial: 

The program will send the vehicle information such as angle and obstacle ahead to the rosseral. and A subscriber will prompt the user for next vehicle movement 1.Forward 2.Turn Left 3.Turn Right
According to the vehicle data (angle and vision), the output could be : 1. Safe and okay to move (green led) 2.Move with caution (yellow led) 3. HIGH RISK (buzzer alarm)
the following algorithm is used:

if the distance to obstacle is less than 7, alert the user for any movement. 
if the distance is clear (>7):
	1. If angle is 0: move anywhere safely.
	2. if angle is 45 or -45 : turn to that direction with caution (yellow led), and move in other directions safely (green led)
	3. if angle is -90 or 90: critical condition! alert the user for any movement ! (buzzer)





--- Circuitry

The ciruitry of the above system can be found in the coursework folder titled circuit.jpg .






--- Implementation updates:

Update 1: Arduini software runs ultrasonic function and publishes the output (distance) in meters on ros serial.

update 2: added angle measurements to adjust to 5 levels ranging from -90 to 90 degrees with step of 45 degrees. increased the string size to include other digits.

update 3: added a subscriber to the same node, and taking the values from rosserial as an integer and verifying if the action requested by the user is possible or not. a package for the project os created with the launch file. 

update 4: synchronized data and fixed some bugs. tested with rosserial.




--- How to: 

1. Set up your circuitry as shown in figure circuit.jpg.
2. download the arduino software VehicleIMU to your arduino.
3. Open a terminal and run roscore.
4. open a new terminal and type rosrun rosserial_python serial_node.py /dev/ttyUSB0 (note: the device maybe at different com)
5. open a new terminal and listen to the chatter by typing: rostopic echo chatter
6. open a new terminal for user interaction and type: rostopic pub my_topic std_msgs/Int32 3 ( the last digit 3 indicates the users input)


