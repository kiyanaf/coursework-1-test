// reference to https://github.com/surabhi96/Library-navigating-robot/wiki/Ultrasonic-sensor-with-ROS
/*
   the following program will use the hc sr04 ultrasonic function to measure
   the distance through sending an 40kHz signal and calculating the distance by
   measuring the time it takes for the signalto return. that value is then divided by the
   speed of sound in air. the output values are not shown on arduino serial monitor but they
   are sent to ros serial  publisher.
*/





// including the required libraries to use the ros
#include <ros.h>
#include <std_msgs/String.h>

// accelerometer pin
#define accelerometer A6

// defines pins numbers
const int trigPin  = 9;
const int echoPin  = 10;

//create a rose node
ros::NodeHandle nh;

// data type string, send to chatter
std_msgs::String str_msg;
ros::Publisher chatter("chatter", &str_msg);



void setup() {
  nh.initNode();
  nh.advertise(chatter);
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
  pinMode(accelerometer, INPUT);//Accelerometer as input
  Serial.begin(57600); // Starts the serial communication
}



void loop() {

  //float distance;
  int angle;
  //read ultrasonic distamce
  int distance = (int) measure (trigPin, echoPin);
  //read the angle from accelerometer
  angle = analogRead(accelerometer);
  angle = map (angle, 400, 630, 0, 5);
  angle = (angle-2)*45;
  //publishing data
  //Serial.println(angle);
  //Serial.println(distance);
  // convert to string
  char cstr[25];
  itoa(angle, cstr, 10);

  //convert to string
  char dist[10];
  itoa(distance, dist, 10);

  //concatanate the data of distance and angle
  strcat(cstr, " | distance(cm): ");
  strcat(cstr, dist);


  // test printing on serial monitor
  Serial.print("Angle :");
  Serial.println(cstr);


  //send data
  str_msg.data = cstr;
  chatter.publish( &str_msg );
  nh.spinOnce();
  delay(1000);

}





// a function that calculates the distance
float measure (int trigPin, int echoPin) {
  long duration;
  float distance;

  // Clears the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);

  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);

  // Calculating the distance in meters
  distance = duration * 0.034 / 2 ;
  //publishing data


  // Prints the distance on the Serial Monitor
  //Serial.print("Distance: ");
  //Serial.println(distance);

  delay(100);
  return distance;
}
